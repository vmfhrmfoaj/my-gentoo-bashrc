#!/bin/bash
# (C) Martin V\"ath <martin at mvath.de>
# SPDX-License-Identifier: GPL-2.0-only

# Portage explicitly unsets all CCACHE_* variables in each phase.
# Therefore, we save them to BASHRCD_CCACHE_* in the setup phase;
# in all later phases, we restore CCACHE_* from these variables
CcachePrepareForTmpfs() {
    if [[ ${FEATURES} == *ccache* ]]; then
        export CCACHE_DIR="${CCACHE_BASEDIR}/ccache"

        echo '>>> Prepare ccache.'
        rm -rf "${CCACHE_DIR}"
        local b="${CCACHE_BACKUP_DIR}/${CATEGORY}/${PN}"
        if [ -d "${b}" ]; then
            cp -ap "${b}" "${CCACHE_DIR}"
        fi
        mkdir -p "${CCACHE_DIR}"
        chown root:portage "${CCACHE_DIR}"
        chmod 2775 "${CCACHE_DIR}"
        touch "${CCACHE_DIR}"/test
    fi
}

CcacheSetup() {
	local i
	: ${CCACHE_BASEDIR=${PORTAGE_TMPDIR:-/var/tmp}}
	: ${CCACHE_SLOPPINESS='file_macro,time_macros,include_file_mtime,include_file_ctime,file_stat_matches,pch_defines'}
	: ${CCACHE_COMPRESS=true}
	if BashrcdTrue $USE_NONGNU && BashrcdTrue $CCACHE_CPP2_OPTIONAL
	then	: ${CCACHE_CPP2=true}
	fi

    CcachePrepareForTmpfs

	# Default to NOHASHDIR unless contrary is specified
	BashrcdTrue "${CCACHE_HASHDIR-}" || CCACHE_NOHASHDIR=true
	for i in ${!CCACHE_*}
	do	if eval "BashrcdTrue \$$i"
		then	eval BASHRCD_$i=\$$i
			export $i
		else	unset $i
		fi
	done

CcacheRestore() {
	local i j
	unset ${!CCACHE_*}
	for i in ${!BASHRCD_CCACHE_*}
	do	j=${i##BASHRCD_}
		eval $j=\$$i
		export $j
	done
}
}
CcacheRestore() {
:
}

CcacheTeardownForTmpfs() {
    CcacheRestore

    if [[ ${FEATURES} == *ccache* ]]; then
        echo '>>> Backup ccache.'
        local b="${CCACHE_BACKUP_DIR}/${CATEGORY}/${PN}"
        mkdir -p "${b}"
        cp -apu "${CCACHE_DIR}"/* "${b}"/
        rm -rf "${CCACHE_DIR}"
    fi
}

# Register CcacheRestore before CcacheSetup to save time in setup phase
BashrcdPhase all CcacheRestore
BashrcdPhase setup CcacheSetup
BashrcdPhase postinst CcacheTeardownForTmpfs
