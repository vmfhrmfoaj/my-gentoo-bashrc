ExportFunc() {
    function strip-flags() {
        echo -n '';
    }
    export -f strip-flags

    function filter-flags() {
        echo -n '';
    }
    export -f filter-flags

    function append-flags() {
        [[ $# -eq 0 ]] && return 0
        case " $* " in
            *' '-[DIU]*) eqawarn 'please use append-cppflags for preprocessor flags' ;;
            *' '-L*|\
            *' '-Wl,*)  eqawarn 'please use append-ldflags for linker flags' ;;
        esac
        export CFLAGS="$* ${CFLAGS}"
        export CXXFLAGS="$* ${CXXFLAGS}"
        export FFLAGS="$* ${FFLAGS}"
        export FCFLAGS="$* ${FCFLAGS}"
    }
    export -f append-flags

    function append-cflags() {
        [[ $# -eq 0 ]] && return 0
        export CFLAGS="$* ${CFLAGS}"
	return 0
    }
    function append-cxxflags() {
        [[ $# -eq 0 ]] && return 0
        export CXXFLAGS="$* ${CXXFLAGS}"
	return 0
    }
    function append-cppflags() {
        [[ $# -eq 0 ]] && return 0
        export CPPFLAGS="$* ${CPPFLAGS}"
	return 0
    }
    function append-fflags() {
        [[ $# -eq 0 ]] && return 0
        export FFLAGS="$* ${FFLAGS}"
        export FCFLAGS="$* ${FCFLAGS}"
	return 0
    }
    export -f append-cflags
    export -f append-cxxflags
    export -f append-cppflags
    export -f append-fflags

    function replace-flags() {
        echo -n '';
    }
    export -f replace-flags
}

BashrcdPhase all ExportFunc
