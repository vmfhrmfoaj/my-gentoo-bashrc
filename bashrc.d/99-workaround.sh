FixForPermissionError() {
    local distccdir="${PORTAGE_TMPDIR:-/var/tmp}"/portage/.distcc
    if [ -d "${distccdir}" ]; then
        chown -R portage:portage "${distccdir}"
    fi
}

BashrcdPhase setup FixForPermissionError
